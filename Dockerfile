#############################################################################
# Copyright (c) 2019 Accenture.
# All rights reserved.
#
# This source code and any compilation or derivative thereof is the
# proprietary information of Accenture and is confidential in nature.
#
# -----------------------------------------------------
# http://www.accenture.com
# -----------------------------------------------------
#############################################################################

#This file helps to create the docker container

##############################################################################
# Import modules
##############################################################################

#Python module in the new created docker container
FROM python:3

#Installing prerequisite for python executable.
RUN pip install \
  requests \
  bs4
ADD HowWarmIsHetInDelft_Roohi.py /
CMD [ "python" , "./HowWarmIsHetInDelft_Roohi.py" ]