#############################################################################
# Copyright (c) 2014-2019 Accenture.
# All rights reserved.
#
# This source code and any compilation or derivative thereof is the
# proprietary information of Accenture and is confidential in nature.
#
# -----------------------------------------------------
# http://www.accenture.com
# -----------------------------------------------------
# Author : Roohi Mahashabde
#############################################################################

"""This module is to print the current temperature in Delft city"""

##############################################################################
# Import modules
##############################################################################

from bs4 import BeautifulSoup
import requests,webbrowser

headers = {
'User-Agent': 'Mozilla/5.0 (Windows NT6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1'
}

url = 'https://weerindelft.nl/clientraw.txt' #Page contains the information of temperature.
res = requests.get(url)
soup = BeautifulSoup(res.content,'html.parser')
temp = str(soup).split()[4] #4th argument is Temperature

final_temperature = round(float(temp))# Rounding up the Float value.

print("Temperature is " + str(final_temperature) + " Degree Celsius")