* Author:         Roohi M Mahashabde
* Date:           03/12/2019
* Purpose         To display the current temperature in Delft city
* Instructions:   To run job manually by clicking Run Pipeline button here: https://gitlab.com/rajeshwari.mahashabde/roohi_assignment1/pipelines